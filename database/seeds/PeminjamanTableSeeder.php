<?php

use Illuminate\Database\Seeder;

class PeminjamanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('peminjaman')->insert([
            [
                'id' => '001',
                'user_id' => 'Notebook',
                'item_id' => '001',
                'item_returned' => 'Notebook',
                'time_added' => '001',
                'time_update' => 'Notebook',
            ],
            [
                'id' => '001',
                'user_id' => 'Notebook',
                'item_id' => '001',
                'item_returned' => 'Notebook',
                'time_added' => '001',
                'time_update' => 'Notebook',
            ],
        ]);
    }
}
