<?php

use Illuminate\Database\Seeder;

class DeviceElektronikTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('device_elektronik')->insert([
            [
                'id' => '011',
                'type_id' => '002',
                'brand' => 'Samnyung',
                'series' => '1234',
                'year' => '2012',
            ],
            [
                'id' => '012',
                'type_id' => '001',
                'brand' => 'Zony Eric',
                'series' => '5678',
                'year' => '2008',
            ],
            [
                'id' => '013',
                'type_id' => '004',
                'brand' => 'Miklotiq',
                'series' => '9012',
                'year' => '2001',
            ],
        ]);
    }
}
