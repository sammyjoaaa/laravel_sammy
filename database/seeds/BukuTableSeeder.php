<?php

use Illuminate\Database\Seeder;

class BukuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku')->insert([
            [
                'id' => '1',
                'author' => 'Bianglala',
                'year' => '2018',
                'genre_id' => '02',
            ],
            [
                'id' => '2',
                'author' => 'Teri Liyat',
                'year' => '2017',
                'genre_id' => '01',
            ],

        ]);
    }
}
