<?php

use Illuminate\Database\Seeder;

class DeviceElektronikTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('device_elektronik_type')->insert([
            [
                'id' => '001',
                'type' => 'Notebook',
            ],
            [
                'id' => '002',
                'type' => 'Tablet',
            ],
            [
                'id' => '003',
                'type' => 'Router',
            ],
            [
                'id' => '004',
                'type' => 'Swicth',
            ],
            [
                'id' => '005',
                'type' => 'RTU',
            ],

        ]);
    }
}
