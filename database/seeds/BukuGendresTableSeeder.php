<?php

use Illuminate\Database\Seeder;

class BukuGendresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku_genres')->insert([
            [
                'id' => '01',
                'genre' => 'Science',
            ],
            [
                'id' => '02',
                'genre' => 'Psychology',
            ],

        ]);
    }
}
